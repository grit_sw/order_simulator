#!/usr/bin/python3
from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer
from logger import logger
import config

broker_url = config.KAFKA_BROKER
schema_registry = config.SCHEMA_REGISTRY

value_schema_str = """
{
   "namespace": "order.complete",
   "name": "value",
   "type": "record",
   "fields" : [
     {
       "name" : "meterid",
       "type" : "string"
     }
   ]
}
"""

topic = 'order-complete'
value_schema = avro.loads(value_schema_str)
value = {"meterid": "test"}

avroProducer = AvroProducer({
    'bootstrap.servers': broker_url,
    'group.id': 'groupid',
    'schema.registry.url': schema_registry
    }, default_value_schema=value_schema)

avroProducer.produce(topic=topic, value=value)
avroProducer.flush()

logger.info("Order has been serviced with topic: '{}'".format(topic))