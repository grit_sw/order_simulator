#!/usr/bin/python3
from confluent_kafka.avro import CachedSchemaRegistryClient as SchemaRegistry
from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer
from confluent_kafka.avro import AvroConsumer
from confluent_kafka import KafkaError
from confluent_kafka.avro.serializer import SerializerError
from avro import schema
import datetime as dt
from datetime import datetime
from time import sleep
import random
from  meterMessage import device_message
import config

def main():
    avroConsumer = consumer()
    avroProducer = producer()
    print("Starting Probe Stream.....")
    while True:
        msg = listenForOrders(avroConsumer)
        if msg == None:
            print(">", end="", flush=True)
            continue
        
        print("\nNew message: {}".format(msg.value()))
        streamMessages(msg.value(), avroProducer)
    
    avroConsumer.close()
        

def listenForOrders(avroConsumer):
    """
    Function listening for validated orders
    """
    try:
        msg = avroConsumer.poll(5)

    except SerializerError as e:
        print("Message deserialization failed for {}: {}".format(msg, e))
        exit()

    if msg is None:
        return

    if msg.error():
        if msg.error().code() == KafkaError._PARTITION_EOF:
            return
        else:
            print(msg.error())
            exit()
    
    return msg


def streamMessages(msg, avroProducer):

    topic = 'probe-stream'

    start = msg['start']
    stop = msg['stop']
    date_form = "%Y-%m-%dT%H:%M:%S.%f"

    start = datetime.strptime(start, date_form)
    stop = datetime.strptime(stop, date_form)
    diff = int((stop-start).total_seconds())
    
    
    probeID = msg['probeID']
    sourceNames = msg['sourceNames']
    for i in range(0, diff, 5):
        msgTime = start+dt.timedelta(seconds =i)

        msgTime = str(msgTime.isoformat())
        device_msg = device_message(msgTime, probeID, sourceNames)

        avroProducer.produce(topic=topic, value=device_msg)
        avroProducer.flush()
        print("Probe stream updated at {}\n".format(msgTime))
        sleep(5)
        

def producer():
    broker_url = config.KAFKA_BROKER
    schema_subject = "probe_streams"
    schema_registry = config.SCHEMA_REGISTRY
    schema_file = 'probe_new.avsc'

    _, sch, _ = SchemaRegistry(schema_registry).get_latest_schema(schema_subject)
    # print(sch)
    if not sch:
        with open(schema_file, 'r+') as sc:
            avro_schema = schema.Parse(sc.read())
            schemareg = SchemaRegistry(schema_registry)
            schemareg.register(schema_subject, avro_schema)
        print('New schema registration at {} with subject {}\n'.format(schema_registry, schema_subject))
        _, sch, _ = SchemaRegistry(schema_registry).get_latest_schema(schema_subject)
    else:
        print('Schema already registered at {} with subject {}\n'.format(schema_registry, schema_subject))
    
    avroProducer = AvroProducer({
        'bootstrap.servers': broker_url,
        'group.id': 'groupid',
        'schema.registry.url': schema_registry
    }, default_value_schema=sch)

    return avroProducer

def consumer():
    avroConsumer = AvroConsumer({
        'bootstrap.servers': 'localhost:9092',
        'group.id': 'groupid',
        'schema.registry.url': 'http://localhost:8081'})
    
    topic = ['validated-order']
    avroConsumer.subscribe(topic)

    return avroConsumer

if __name__=='__main__':
    try:
        print("...GTG Simulator...")
        main()
    except KeyboardInterrupt:
        pass
    # msg = {"probeID":"probe1",
    #     "start":"2018-12-06T13:28:45.966762",
    #     "stop":"2018-12-06T13:30:45.966762"}

    # streamMessages(msg)