from random import uniform

def device_message(order_time, probeID, sourceNames):
	n = {
		u'master': {
			u'configuration_IDs': [u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'],
			u'id': probeID,
			u'time': order_time
		},
		u'data': [{
			# u'activeSource': [
            #     {
            #         u'sourceName': u'MUST4024C', u'sourceType': u'inverter', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'
			# 	}],
			u'powerSinceLast': [
				{u'sourceName': src['sourceName'], u'powerSinceLast': round(uniform(1000, 20000), 4), u'sourceType': src['sourceType'], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'} for src in sourceNames
				# {u'sourceName': u'MUST4024C', u'powerSinceLast': round(uniform(1000, 20000), 4), u'sourceType': u'inverter', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
				# {u'sourceName': u'PERKINSP40P3', u'powerSinceLast': round(uniform(1000, 20000), 4), u'sourceType': u'generator', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
				# {u'sourceName': u'EKODISCO', u'powerSinceLast': round(uniform(1000, 20000), 4), u'sourceType': u'grid', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}
			],
			# u'costSinceLast': [
				# {u'sourceName': u'MUST4024C', u'sourceType': u'inverter', u'costSinceLast': round(uniform(0, 4), 4), u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
				# {u'sourceName': u'PERKINSP40P3', u'sourceType': u'generator', u'costSinceLast': round(uniform(0, 4), 4), u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
				# {u'sourceName': u'EKODISCO', u'sourceType': u'grid', u'costSinceLast': round(uniform(0, 4), 4), u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}
				# ],
			u'energySinceLast': [
                {u'sourceName': src['sourceName'], u'energySinceLast': round(uniform(1000, 20000), 4), u'sourceType': src['sourceType'], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'} for src in sourceNames
				# {u'sourceName': u'MUST4024C', u'energySinceLast': round(uniform(0, 4), 4), u'sourceType': u'inverter', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
				# {u'sourceName': u'PERKINSP40P3', u'energySinceLast': round(uniform(0, 4), 4), u'sourceType': u'generator', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
				#  {u'sourceName': u'EKODISCO', u'energySinceLast': round(uniform(0, 4), 4), u'sourceType': u'grid', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}
				 ],
			u'energyTodaySource': [
                {u'sourceName': src['sourceName'], u'energyToday': round(uniform(1000, 20000), 4), u'sourceType': src['sourceType'], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'} for src in sourceNames
				# {u'sourceName': u'MUST4024C', u'energyToday': round(uniform(1000, 4000), 4), u'sourceType': u'inverter', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
				# {u'sourceName': u'PERKINSP40P3', u'energyToday': round(uniform(1000, 4000), 4), u'sourceType': u'generator', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
				# {u'sourceName': u'EKODISCO', u'energyToday': round(uniform(1000, 4000), 4), u'sourceType': u'grid', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}
				],
            u'timeTodaySource': [
				{u'sourceName': src['sourceName'], u'timeToday': round(uniform(1000, 20000), 4), u'sourceType': src['sourceType'], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'} for src in sourceNames
                # {u'sourceName': u'MUST4024C', u'sourceType': u'inverter', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', u'timeToday': round(uniform(0, 24), 4)},
                # {u'sourceName': u'PERKINSP40P3', u'sourceType': u'generator', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', u'timeToday': round(uniform(0, 24), 4)},
                # {u'sourceName': u'EKODISCO', u'sourceType': u'grid', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', u'timeToday': round(uniform(0, 24), 4)}
                ],
            # u'dod': [
            #     {u'sourceName': u'MUST4024C', u'sourceType': u'inverter', u'dod': round(uniform(0, 4), 4), u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}
            #     ],
            # u'batteryCapLast': [
            #     {u'sourceName': u'MUST4024C', u'sourceType': u'inverter', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF', u'batteryCapLast': round(uniform(10, 400), 4)}
            #     ],
            # u'current': [
            #     {u'sourceName': u'MUST4024C', u'sourceType': u'inverter', u'value': [round(uniform(0, 4), 4), 0.0, 0.0], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
            #     {u'sourceName': u'PERKINSP40P3', u'sourceType': u'generator', u'value': [0.0, 0.0, round(uniform(0, 4), 4)], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
            #     {u'sourceName': u'EKODISCO', u'sourceType': u'grid', u'value': [round(uniform(0, 4), 4), round(uniform(0, 4), 4), round(uniform(0, 4), 4)], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}
            #     ],
            # u'fuelSinceLast': [],
            # u'fuelHourRate': [{
            #     u'sourceName': u'PERKINSP40P3', u'fuelHourRate': 0.0, u'sourceType': u'generator', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}],
            # u'voltage': [
            #     {u'sourceName': u'MUST4024C', u'sourceType': u'inverter', u'value': [230.0, 0.0, 0.0], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
            #     {u'sourceName': u'PERKINSP40P3', u'sourceType': u'generator', u'value': [0.0, 0.0, 230.0], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
            #     {u'sourceName': u'EKODISCO', u'sourceType': u'grid', u'value': [230.0, 230.0, 230.0], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}
            # ],
            u'time': order_time,
            u'id': probeID,#u'B8:27:EB:26:8F:40',
            # u'Type': u'1',
            # u'powerfactor': [
            #     {u'sourceName': u'MUST4024C', u'sourceType': u'inverter', u'value': [round(uniform(0.8, 1), 4), 0.0, 0.0], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
            #     {u'sourceName': u'PERKINSP40P3', u'sourceType': u'generator', u'value': [0.0, 0.0, round(uniform(0.8, 1), 4)], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
            #     {u'sourceName': u'EKODISCO', u'sourceType': u'grid', u'value': [round(uniform(0.8, 1), 4), round(uniform(0.8, 1), 4), round(uniform(0.8, 1), 4)], u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}],

            # u'costTodaySource': [{u'sourceName': u'MUST4024C', u'costToday': round(uniform(10, 10000), 4), u'sourceType': u'inverter', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
            # {u'sourceName': u'PERKINSP40P3', u'costToday': round(uniform(10, 10000), 4), u'sourceType': u'generator', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'},
            # {u'sourceName': u'EKODISCO', u'costToday': round(uniform(10, 10000), 4), u'sourceType': u'grid', u'configID_FK': u'G1OKQR2XWWH5DZFY3Z4MCUTXIAEZ1OJF'}]
        }]}
	return n