FROM alpine:latest

WORKDIR /home/user/

COPY requirements.txt requirements.txt
RUN apk update
RUN apk add python3 librdkafka-dev
RUN apk add --virtual .build-deps \
    gcc \
    musl-dev \
    python3-dev && \
    python3 -m venv venv && \
    venv/bin/pip install -r requirements.txt --no-cache-dir && \
    apk --purge del .build-deps

COPY sellOrder.py buyOrder.py order.avsc config.py logger.py orderComplete.py new_buy_order.avsc new_sell_order.avsc start.sh ./

RUN chmod +x start.sh
ENTRYPOINT [ "./start.sh" ]