#!/usr/bin/python3
from confluent_kafka.avro import CachedSchemaRegistryClient as SchemaRegistry
from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer
from avro import schema
from datetime import timedelta
from datetime import datetime
import sys
from logger import logger
import config

def message(days, seconds):
    # probeID = u'B8:27:EB:26:8F:40'
    # ID = u'2'
    # kwOrder = 200
    # sourceNames = [{u'sourceName': u'PERKINSP40P3', u'sourceType': u'generator'}]
    t = datetime.now()
    start = str(t.isoformat())
    stop = str((t+timedelta(days=days, seconds=seconds)).isoformat())
    # # stop = str(datetime(t.year, t.month, t.day, t.hour, t.minute+2, ).isoformat())
    
    # msg = {"ID": ID, "kwOrder": kwOrder, "stop": stop, "probeID": probeID, "start": start, "sourceNames": sourceNames}
    
    msg = {
        'user_group_id': 'test', 
        'prosumer_id': 'test', 
        'order_id': 'test', 
        'start_date_time': start, 
        'end_date_time': stop, 
        'rate': 10, 
        'power_limit': 12000.0, 
        'energy_limit': 2000.0, 
        'auto_renew': True, 
        'renew_frequency': '1', 
        'billing_unit_id': 'test'
    }

    return msg

def sellOrder(days=0, seconds=120):

    topic = 'new-sell-order'
    broker_url = config.KAFKA_BROKER
    schema_subject = "raw_sell_orders"
    schema_registry = config.SCHEMA_REGISTRY
    schema_file = config.SELL_ORDER_SCHEMA

    _, sch, _ = SchemaRegistry(schema_registry).get_latest_schema(schema_subject)
    if not sch:
        with open(schema_file) as sc:
            avro_schema = schema.Parse(sc.read())
            schemareg = SchemaRegistry(schema_registry)
            schemareg.register(schema_subject, avro_schema)
        logger.info('New schema registration  {} with subject {}\n'.format(schema_registry, schema_subject))
        _, sch, _ = SchemaRegistry(schema_registry).get_latest_schema(schema_subject)
    else:
        logger.info('Schema already registered at {} with subject {}\n'.format(schema_registry, schema_subject))
    
    avroProducer = AvroProducer({
        'bootstrap.servers': broker_url,
        'group.id': 'groupid',
        'schema.registry.url': schema_registry
    }, default_value_schema=sch)

    msg = message(days, seconds)
    avroProducer.produce(topic=topic, value=msg)
    avroProducer.flush()
    logger.info("Sell order created to topic '{}'".format(topic))
    logger.info(msg)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        try:
            days = int(sys.argv[1])
            seconds = int(sys.argv[2])
        except ValueError:
            logger.info("Arguments must be numeric")
            exit()
        except IndexError:
            sellOrder(days)
        else:
            sellOrder(days, seconds)
    else:
        sellOrder()
