## GTG SIMULATOR

The files can be to simulate the buy orders and sell orders.

### Required tools

1. Python >=3.5.2
2. librdkafka 
2. Confluent-Kafka == 2.11

### How to use

* buyOrder.py - used to create a new buy order with a 2 minute timeline. To change the timeline, provide values for day and seconds as command line args.

* sellOrder.py - used to create a new sell order with a 2 minute timeline. To change the timeline, provide values for day and seconds as command line args.

* probeStream.py - listens for new orders, then stream energy usage according to the timeline in the order at 5 seconds interval

* consumer.py - displays the messages for a given topic specified as a command line arg

* probe_new.avsc, order.avsc - avro schema for the message data formats

* meterMessage.py - used to create meter sample data with random data 

* orderComplete.py - used to simulate the meter response for a completed order