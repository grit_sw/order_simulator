#!/usr/bin/python3

from confluent_kafka import KafkaError
from confluent_kafka.avro import AvroConsumer
from confluent_kafka.avro.serializer import SerializerError
import sys

def consumer(topic='probe-stream'):
    c = AvroConsumer({
        'bootstrap.servers': 'localhost:9092',
        'group.id': 'groupid',
        'schema.registry.url': 'http://localhost:8081'})

    c.subscribe([topic])

    print("Consumer started for topic: {}".format(topic))
    print("\n.....................\n")
    
    while True:
        try:
            msg = c.poll(5)

        except SerializerError as e:
            print("Message deserialization failed for {}: {}".format(msg, e))
            break

        if msg is None:
            continue

        if msg.error():
            if msg.error().code() == KafkaError._PARTITION_EOF:
                continue
            else:
                print(msg.error())
                break

        print(msg.value())
        print("\n.....................\n")
    

    c.close()

if __name__ == "__main__":
    try:
        if len(sys.argv) > 1:
            consumer(sys.argv[1])
        else:
            consumer()
    except KeyboardInterrupt:
        pass