#!/usr/bin/python3

KAFKA_BROKER='broker:9092'
SCHEMA_REGISTRY='http://schema-registry:8081'
SCHEMA_FILE='order.avsc'
BUY_ORDER_SCHEMA='new_buy_order.avsc'
SELL_ORDER_SCHEMA='new_sell_order.avsc'