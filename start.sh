#!/bin/sh

source venv/bin/activate

echo -e "Simulating a Sell Order..."
python3 sellOrder.py

echo -e "\nSimulating a Buy Order..."
python3 buyOrder.py

echo -e "\nSimulating a Completed Order..."
python3 orderComplete.py